# youtube-tool

CLI tool for easy downloading and opening of videos from YouTube (presumes presence of [yt-dlp](https://github.com/yt-dlp/yt-dlp) and [xdg-open](https://freedesktop.org/wiki/Software/xdg-utils/)).

```sh
$ yt
YouTube download utility by *monnef* (rewritten in Haskell from Clojure)
Using URL from clipboard - https://rumble.com/vtcrad-jordan-peterson-causes-meltdown-over-joe-rogan-podcast-appearance-hilarious.html
Running command: yt-dlp --no-playlist --no-check-certificate --force-ipv4 --no-call-home --newline --sponskrub https://rumble.com/vtcrad-jordan-peterson-causes-meltdown-over-joe-rogan-podcast-appearance-hilarious.html
[generic] vtcrad-jordan-peterson-causes-meltdown-over-joe-rogan-podcast-appearance-hilarious: Requesting header
[generic] vtcrad-jordan-peterson-causes-meltdown-over-joe-rogan-podcast-appearance-hilarious: Downloading webpage
[generic] vtcrad-jordan-peterson-causes-meltdown-over-joe-rogan-podcast-appearance-hilarious: Extracting information
[RumbleEmbed] vqql6r: Downloading JSON metadata
[info] vqql6r: Downloading 1 format(s): mp4-1080p
[download] Jordan Peterson Causes MELTDOWN Over Joe Rogan Podcast Appearance! (Hilarious) [vqql6r].mp4 has already been downloaded
[download] 100% of 272.22MiB
[SponSkrub] Skipping sponskrub since it is not a YouTube video

WARNING: [generic] Falling back on generic information extractor.

Running command: yt-dlp --no-playlist --no-check-certificate --force-ipv4 --no-call-home --newline --sponskrub --no-playlist --get-filename https://rumble.com/vtcrad-jordan-peterson-causes-meltdown-over-joe-rogan-podcast-appearance-hilarious.html
Open downloaded video /home/moen/Downloads/yt/Jordan Peterson Causes MELTDOWN Over Joe Rogan Podcast Appearance! (Hilarious) [vqql6r].mp4? (y/n) [y]:
```
