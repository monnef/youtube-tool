{-# LANGUAGE TypeOperators #-}

-- |
-- Module     : Main
-- Description: CLI tool for easy downloading and opening of videos from YouTube. Depends on yt-dlp, sponskrub, xdg-open.
-- Copyright  : (c) monnef, 2021-2024
-- License    : GPL-3
-- Stability  : experimental
module Main where

import Control.Monad (when, join)
import Data.Either.Utils (fromRight)
import Data.Function ((&))
import Data.Functor ((<&>))
import Data.List (intercalate, isInfixOf, isPrefixOf)
import Lib
import System.Directory (doesFileExist, getHomeDirectory)
import System.Exit (ExitCode (ExitFailure, ExitSuccess))
import System.FilePath.Posix (takeBaseName, takeExtension)
import System.Hclip (getClipboard)
import System.IO (BufferMode (NoBuffering), hFlush, hGetContents, hSetBuffering, hSetEcho, stdout)
import System.Process (StdStream (CreatePipe), createProcess, cwd, proc, std_err, std_out, waitForProcess)
import Text.Regex.PCRE.String (compBlank, compile, execBlank, regexec)
import Data.Map (Map)
import qualified Data.Map as M
import Data.Maybe (catMaybes)

-- config

downloadCommand :: [String]
downloadCommand = ["yt-dlp"]
--downloadCommand = ["python", "-m", "yt_dlp"]

downloadCommandDir :: String
downloadCommandDir = "."
--downloadCommandDir = "/mnt/dev/python/yt-dlp"

extraArgs :: [String]
extraArgs = ["--extractor-args", "youtube:player_client=android,web"]

extraArgsByService :: Map String [String]
extraArgsByService = M.fromList [
    ("nebula", ["--cookies-from-browser", "vivaldi"]),
    ("curiositystream", ["--cookies-from-browser", "vivaldi", "-f", "bv+ba[language=en]"])
  ]

-- // config


runCmd :: [String] -> Bool -> String -> IO ([String], [String], ExitCode)
runCmd (cmd:args) printOutput targetDir = do
  putStrAndFlush $ "Running command: " ++ intercalate " " (cmd : args) ++ "\n"
  (_, Just hout, Just herr, jHandle) <-
    createProcess
      (proc cmd args)
        { cwd = Just targetDir,
          std_out = CreatePipe,
          std_err = CreatePipe
        }

  --  when printOutput $ hSetEcho hout True
  --  hSetBuffering hout NoBuffering
  out <- hGetContents hout
  when printOutput $ putStrLn out

  err <- hGetContents herr
  when printOutput $ putStrLn err

  exitCode <- waitForProcess jHandle
  return (lines out, lines err, exitCode)

runCmdInDownloadDir :: String -> [String] -> Bool -> IO ([String], [String], ExitCode)
runCmdInDownloadDir cmd args printOutput = do
  dir <- downloadDir
  runCmd (cmd:args) printOutput dir

getExtraArgsByService :: String -> [String]
getExtraArgsByService link = M.keys extraArgsByService & filter (`isInfixOf` link) <&> flip M.lookup extraArgsByService & catMaybes & join

downloadArgs :: [String]
downloadArgs = ["--no-playlist", "--no-check-certificate", "--force-ipv4", "--no-call-home", "--newline", "--sponsorblock-mark", "all", "-v"] <> extraArgs

pathArgs :: IO [String]
pathArgs = do
  downDir <- downloadDir
  return ["--paths", "home:" <> downDir, "--paths", "temp:" <> downDir]

downloadVideo :: String -> IO ([String], [String], ExitCode)
downloadVideo link = do
  dir <- downloadDir
  pathArgs' <- pathArgs
  (out, err, exitCode) <- runCmd (downloadCommand ++ downloadArgs ++ pathArgs' ++ getExtraArgsByService link ++ [link]) True downloadCommandDir
  case exitCode of
    ExitFailure errCode -> error $ "Error " ++ show errCode ++ " occured. Error output:\n" ++ unlines err
    ExitSuccess -> return (out, err, exitCode)

getVideoFilename :: String -> IO String
getVideoFilename link = do
  dir <- downloadDir
  pathArgs' <- pathArgs
  (out, err, exitCode) <- runCmd (downloadCommand ++ downloadArgs ++ pathArgs' ++ getExtraArgsByService link ++ ["--no-playlist", "--get-filename", link]) False downloadCommandDir
  case exitCode of
    ExitFailure errCode ->
      error $ "Error " ++ show errCode ++ " during getting filename occured. Error output:\n" ++ unlines err
    ExitSuccess -> onRunSuccess $ head out
  where
    onRunSuccess :: String -> IO String
    onRunSuccess returnedFileName = do
      returnedFileExists <- doesFileExist returnedFileName
      if returnedFileExists
        then return returnedFileName
        else tryOtherExtensions returnedFileName
    tryOtherExtensions :: String -> IO String
    tryOtherExtensions origName = do
      let otherExtensions = ["webm", "mkv", "avi", "mp4", "mp3"]
      isExistingList <- mapM mapFn otherExtensions :: IO [(Bool, String)]
      let onlyExisting = isExistingList & filter fst & map snd :: [String]
      when (null onlyExisting) $ error $ "Newly created file not found even when trying other extensions." ++ show isExistingList
      return $ head onlyExisting
      where
        baseName = takeBaseName origName
        mapFn ext = do
          dir <- downloadDir
          let newName = dir ++ "/" ++ baseName ++ "." ++ ext
          exists <- doesFileExist newName
          return (exists, newName)

openVideo :: String -> IO ()
openVideo name = do
  putStrLn $ "opening file: " ++ show name
  _ <- runCmdInDownloadDir "xdg-open" [name] True
  return ()

homeDir :: IO String
homeDir = getHomeDirectory

downloadDirSuffix :: String
downloadDirSuffix = "/Downloads/yt"

downloadDir :: IO String
downloadDir = homeDir <&> (++ downloadDirSuffix)

putStrAndFlush :: String -> IO ()
putStrAndFlush x = do
  putStr x
  hFlush stdout

printQueryAndReadLine :: IO String
printQueryAndReadLine = do
  putStrAndFlush "link: "
  getLine

looksLikeVideoLink :: String -> Bool
looksLikeVideoLink x = isPrefixOf "http" x && any (`isInfixOf` x) videoProviders
  where
    videoProviders = ["youtube", "odysee", "bitchute", "rumble", "nebula", "curiositystream"]

handleQueryAndFileOpen :: String -> IO ()
handleQueryAndFileOpen link = do
  fileName <- getVideoFilename link
  putStrAndFlush $ "Open downloaded video " ++ fileName ++ "? (y/n) [y]: "
  openAnswer <- getLine
  when (openAnswer == "y" || openAnswer == "") $ do
    putStrLn $ "Opening file: \"" ++ fileName ++ "\""
    openVideo fileName

main :: IO ()
main = do
  putStrLn "YouTube download utility by *monnef* (rewritten in Haskell from Clojure)"

  clipboard <- getClipboard
  link <-
    if looksLikeVideoLink clipboard
      then do
        putStrLn $ "Using URL from clipboard - " ++ clipboard
        return clipboard
      else printQueryAndReadLine

  (out, err, exitCode) <- downloadVideo link
  handleQueryAndFileOpen link
